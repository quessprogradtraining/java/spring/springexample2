package com.example.Demo1;

import com.example.Demo1.component.Employee;
import com.example.Demo1.component.Person;
import com.example.Demo1.component.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class Demo1Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context= SpringApplication.run(Demo1Application.class, args);
		Employee employeeObj=context.getBean(Employee.class);
		System.out.println(employeeObj);
	}

}
