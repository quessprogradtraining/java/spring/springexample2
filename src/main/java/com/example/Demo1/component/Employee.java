package com.example.Demo1.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Employee {
    private int eid;
 @Autowired
  Person personObj;
    public Employee() {
        this.eid=123;
        System.out.println(personObj);
    }

    public Employee(int eid) {
        this.eid = eid;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "eid=" + eid +
                ", personObj=" + personObj +
                '}';
    }
    /* public void displayEmployeeDetails(){
        System.out.println(eid);
        personObj.display();*/

}

