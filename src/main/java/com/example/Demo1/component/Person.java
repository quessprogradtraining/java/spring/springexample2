package com.example.Demo1.component;

import org.springframework.stereotype.Component;

@Component
public class Person {
    private String name;
    private  String city;

    public Person() {
        this.name="bhavana";
        this.city="bangalure";
    }

    public Person(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public void display(){
        System.out.println(name+" "+city);
    }
}
